#!/bin/sh

err=0

agents=$(pcs resource list ocf:heartbeat 2>&1)
if [ $(echo "$agents" | wc -l) -lt 2 ]; then
	echo "ERROR: pcs: agents available:\n$agents"
	err=$((err+1))
else
	echo "INFO: pcs: agents available..."
fi

if [ $err -ge 1 ]; then
	echo -e "\nERROR: $err tests FAILED..."
	exit 1
fi
