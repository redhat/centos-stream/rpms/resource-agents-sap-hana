#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the SAPHanaSR upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="SAPHanaSR-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version 204f1461

%global saphana_prefix SAPHanaSR
%global saphana_version e8188e0

# Whether this platform defaults to using systemd as an init system
# (needs to be evaluated prior to BuildRequires being enumerated and
# installed as it's intended to conditionally select some of these, and
# for that there are only few indicators with varying reliability:
# - presence of systemd-defined macros (when building in a full-fledged
#   environment, which is not the case with ordinary mock-based builds)
# - systemd-aware rpm as manifested with the presence of particular
#   macro (rpm itself will trivially always be present when building)
# - existence of /usr/lib/os-release file, which is something heavily
#   propagated by systemd project
# - when not good enough, there's always a possibility to check
#   particular distro-specific macros (incl. version comparison)
%define systemd_native (%{?_unitdir:1}%{!?_unitdir:0}%{nil \
  } || %{?__transaction_systemd_inhibit:1}%{!?__transaction_systemd_inhibit:0}%{nil \
  } || %(test -f /usr/lib/os-release; test $? -ne 0; echo $?))

# determine the ras-set to process based on configure invokation
%bcond_with rgmanager
%bcond_without linuxha

Name:     resource-agents-sap-hana
Summary:  SAP HANA cluster resource agents
Epoch:    1
Version:  0.162.3
Release:  5%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}.1
License:  GPLv2+
URL:      https://github.com/SUSE/SAPHanaSR
Source0:  %{upstream_prefix}-%{upstream_version}.tar.gz
Source1:  %{saphana_prefix}-%{saphana_version}.tar.gz

Patch0:   RHEL-59660-fix-SAPHanaTopology-regression.patch

BuildArch:  noarch

# Build dependencies
BuildRequires: make
BuildRequires: automake autoconf pkgconfig gcc
BuildRequires: libxslt glib2-devel libqb-devel
BuildRequires: systemd
BuildRequires: which

%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
BuildRequires: python3-devel
%else
BuildRequires: python-devel
%endif

%if 0%{?fedora} || 0%{?centos} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%if 0%{?rhel} == 0
BuildRequires: libnet-devel
%endif
%endif

%if 0%{?suse_version}
BuildRequires:  libnet-devel
BuildRequires:  libglue-devel
BuildRequires:  libxslt docbook_4 docbook-xsl-stylesheets
%endif

Requires:   resource-agents >= 4.8.0
Conflicts:  %{name}-scaleout

Requires: /bin/bash /usr/bin/grep /bin/sed /bin/gawk

%description
The SAP HANA resource agents interface with Pacemaker to allow
SAP instances to be managed in a cluster environment.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%setup -q -T -D -a 1 -n %{upstream_prefix}-%{upstream_version}

# Add patches before moving files
%patch -d %{saphana_prefix}-%{saphana_version} -p1 -P 0

### add SAPHana agents to Makefile.am
mv %{saphana_prefix}-%{saphana_version}/ra/SAPHana* heartbeat

# Find the existing SAPInstance entry in the list and add 2 new after in corresponding formatting.
# heartbeat/Makefile.am indents by 3 tabs in the target list
sed -i -e 's/\(\t\tSAPInstance\t\t\\\)/\1\n\t\t\tSAPHana\t\t\t\\\n\t\t\tSAPHanaTopology\t\\/' heartbeat/Makefile.am

# Find the existing SAPInstance entry in the list and add 2 new after in corresponding formatting.
# doc/man/Makefile.am indents by 26 spaces in the target list
sed -i -e 's/\( \{26\}ocf_heartbeat_SAPInstance.7 \\\)/\1\n'\
'                          ocf_heartbeat_SAPHana.7 \\\n'\
'                          ocf_heartbeat_SAPHanaTopology.7 \\/' doc/man/Makefile.am

# change provider company in hook scripts
sed -i -e 's/\("provider_company": \)"SUSE"/\1"Red Hat"/g' %{saphana_prefix}-%{saphana_version}/srHook/SAPHanaSR.py
sed -i -e 's/\("provider_company": \)"SUSE"/\1"Red Hat"/g' %{saphana_prefix}-%{saphana_version}/srHook/susChkSrv.py

# rename patterns to remove "sus" prefix in hook script
sed -i -e 's/susChkSrv/ChkSrv/g' %{saphana_prefix}-%{saphana_version}/srHook/susChkSrv.py
sed -i -e 's/suschksrv/chksrv/g' %{saphana_prefix}-%{saphana_version}/srHook/susChkSrv.py
sed -i -e 's/sustkover_timeout/tkover_timeout/g' %{saphana_prefix}-%{saphana_version}/srHook/susChkSrv.py

%build
if [ ! -f configure ]; then
    ./autogen.sh
fi

%if 0%{?fedora} >= 11 || 0%{?centos} > 5 || 0%{?rhel} > 5
CFLAGS="$(echo '%{optflags}')"
%global conf_opt_fatal "--enable-fatal-warnings=no"
%else
CFLAGS="${CFLAGS} ${RPM_OPT_FLAGS}"
%global conf_opt_fatal "--enable-fatal-warnings=yes"
%endif

%if %{with rgmanager}
%global rasset rgmanager
%endif
%if %{with linuxha}
%global rasset linux-ha
%endif
%if %{with rgmanager} && %{with linuxha}
%global rasset all
%endif

export CFLAGS

%configure \
%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
    PYTHON="%{__python3}" \
%endif
    %{conf_opt_fatal} \
%if %{defined _unitdir}
    --with-systemdsystemunitdir=%{_unitdir} \
%endif
%if %{defined _tmpfilesdir}
    --with-systemdtmpfilesdir=%{_tmpfilesdir} \
    --with-rsctmpdir=/run/resource-agents \
%endif
    --with-pkg-name=resource-agents \
    --with-ras-set=%{rasset}

make %{_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# remove other agents
find %{buildroot}/usr/lib/ocf ! -type d ! -iname "SAPHana*" -exec rm {} \;
find %{buildroot}/%{_mandir} -type f ! -iname "*SAPHana*" -exec rm {} \;

mkdir -p %{buildroot}/%{_datadir}/SAPHanaSR/srHook
cp -r %{saphana_prefix}-%{saphana_version}/srHook/global.ini %{buildroot}/%{_datadir}/SAPHanaSR/srHook
cp -r %{saphana_prefix}-%{saphana_version}/srHook/SAPHanaSR.py %{buildroot}/%{_datadir}/SAPHanaSR/srHook
cp -r %{saphana_prefix}-%{saphana_version}/srHook/susChkSrv.py %{buildroot}/%{_datadir}/SAPHanaSR/srHook/ChkSrv.py

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%files
%defattr(-,root,root)
%{_usr}/lib/ocf/resource.d/heartbeat/SAPHana*
%{_mandir}/man7/*SAP*
%{_datadir}/SAPHanaSR

%exclude /etc
%exclude /usr/include
%exclude /usr/lib/debug
%exclude /usr/lib/systemd
%exclude /usr/lib/tmpfiles.d
%exclude /usr/libexec
%exclude /usr/sbin/ldirectord
%exclude /usr/sbin/ocf*
%exclude /usr/share/resource-agents
%exclude /usr/share/pkgconfig/resource-agents.pc
%exclude /usr/src

%changelog
* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 1:0.162.3-5.1
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Fri Sep 20 2024 Janine Fuchs <jfuchs@redhat.com> - 0.162.3-5
- Fix regression in SAPHanaTopology agent.
  Resolves: RHEL-59661

* Fri Jun 28 2024 Janine Fuchs <jfuchs@redhat.com> - 0.162.3-4
- Add package to RHEL 10.

  Resolves: RHEL-40574

- Remove dependency on perl.

  Resolves: RHEL-40355

# vim:set ai ts=4 sw=4 sts=4 et:
